namespace ProxyWpfImageTest
{
    using System.Windows.Input;

    //Companion class that contains commands related to MainWindowViewModel
    //I use this approach because passing in MainWindowViewModel into a commmand that is being instantiated from MainWindowViewModel itself
    //creates deadlock situation.
    public static class MainWindowViewModelCommands
    {
        public static ICommand TattooCommand { get; } = new TattooCommand();
    }
}