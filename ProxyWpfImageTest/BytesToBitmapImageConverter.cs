﻿using System;

namespace ProxyWpfImageTest
{
    using System.Globalization;
    using System.IO;
    using System.Windows.Data;
    using System.Windows.Media.Imaging;

    [ValueConversion(typeof(byte[]), typeof(BitmapSource))]
    public class BytesToBitmapImageConverter : IValueConverter
    {
        public static BytesToBitmapImageConverter Default { get; } = new BytesToBitmapImageConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var buffer = (byte[])value;

            try
            {
                return new JpegBitmapDecoder(new MemoryStream(buffer), BitmapCreateOptions.None,
                    BitmapCacheOption.None).Frames[0];
            }
            catch (NotSupportedException)
            {
                //ignore
                return null;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
