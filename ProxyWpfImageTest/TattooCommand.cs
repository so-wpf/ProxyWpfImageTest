namespace ProxyWpfImageTest
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Windows.Input;

    public class TattooCommand : ICommand
    {
        public TattooCommand()
        {
            //though I'm not going to unhook this in demo, you may want to implement IDisposable & add unhook
            //code in real app to unhook perhaps on view unload
            //this code will raise CanExecuteChanged each time ImageSourceBytes changes
            PropertyChangedEventManager.AddHandler(MainWindowViewModel.Current, (s, args) => CanExecuteChanged?.Invoke(s, EventArgs.Empty),
                nameof(MainWindowViewModel.ImageSourceBytes));
        }

        //this command can execute as long as there's a non-null image source byte array
        //this is of course just to demo how you'd do this otherwise you'd have a proper check or even just plain true
        public bool CanExecute(object parameter) => MainWindowViewModel.Current.ImageSourceBytes != null;

        public void Execute(object parameter)
        {
            var buffer = new byte[8192];
            var readCount = 1;

            var inputFile = parameter as string;
            
            using (var outputStream = new MemoryStream())
            using (var inputStream = new FileStream(inputFile, FileMode.Open))
            {
                while (readCount > 0)
                {
                    readCount = inputStream.Read(buffer, 0, buffer.Length);
                    outputStream.Write(buffer, 0, readCount);
                }

                MainWindowViewModel.Current.ImageSourceBytes = outputStream.ToArray();
            }
        }

        public event EventHandler CanExecuteChanged;
    }
}