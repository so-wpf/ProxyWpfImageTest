namespace ProxyWpfImageTest
{
    using System.ComponentModel;
    using JetBrains.Annotations;

    public class MainWindowViewModel : INotifyPropertyChanged
    {
        byte[] _imageSourceBytes;
        public event PropertyChangedEventHandler PropertyChanged;

        //private constructor to ensure we don't accidently create a new instance of this singleton viewmodel
        MainWindowViewModel() { ImageSourceBytes = new byte[0]; }

        public static MainWindowViewModel Current { get; } = new MainWindowViewModel();

        public byte[] ImageSourceBytes
        {
            get { return _imageSourceBytes; }
            set
            {
                if (Equals(value, _imageSourceBytes)) return;
                _imageSourceBytes = value;
                OnPropertyChanged(nameof(ImageSourceBytes));
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}